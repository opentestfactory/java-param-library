<!--

     Copyright (c) 2021 - 2023 Henix, henix.fr

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.

-->
# OpenTestFactory Java Parameter Library

This package is part of the *OpenTestFactory* framework.

## Overview

The *Java Parameter Library* is a library that allows Java-based test automation technologies to retrieve external parameters when they cannot do so natively. 
It can be used for tests run from the *OpenTestFactory* orchestrator or independently.

## Installation

The library is currently available through Maven. To use it, add the following dependency and repository to your Java project's pom.xml:

``` mvn
        <dependency>
            <groupId>org.opentestfactory.util</groupId>
            <artifactId>opentestfactory-java-param-library</artifactId>
            <version>1.0.0.</version>
        </dependency>
```
``` mvn
        <repository>
            <id>org.squashtest.ta.release</id>
            <name>squashtest test automation - releases</name>
            <url>http://repo.squashtest.org/maven2/releases</url>
        </repository>
```

## Use as part of an execution by *OpenTestFactory*

### Retrieving parameters in a test class

* When the test project is run by *OpenTestFactory*, the parameter transfer process is transparent to the user. 
You have to refer to the *OpenTestFactory* documentation to write the PEaC in which you will inform his parameters.
In your test java project, you can then retrieve the value of a parameter of the desired type using the following syntax:

``` java
ParameterService.INSTANCE.getString("paramName");
ParameterService.INSTANCE.getInt("paramName");
ParameterService.INSTANCE.getFloat("paramName");
ParameterService.INSTANCE.getDouble("paramName");
ParameterService.INSTANCE.getBoolean("paramName");
```
* The above methods look for the desired parameter in the *test parameters*; if they cannot find it, they then look for it in the *global parameters*. These methods throw a ``ParameterNotFoundException`` if the parameter is not found. If the parameter is found but cannot be converted to the desired type, a ``ParameterFormatException`` is thrown. Consider handling these exceptions in your test classes. Warning: the conversion of Boolean data returns ``true`` when the character string to be converted is equal to ``"true"`` (whatever the case), ``false`` in all other cases; but never propagates an exception.

* It is also possible to define a default parameter in the case where the parameter is non-existent by using the following syntax:

``` java
ParameterService.INSTANCE.getString("paramName", defaultValue);
ParameterService.INSTANCE.getInt("paramName", defaultValue);
ParameterService.INSTANCE.getFloat("paramName", defaultValue);
ParameterService.INSTANCE.getDouble("paramName", defaultValue);
ParameterService.INSTANCE.getBoolean("paramName", defaultValue);
```
* The above methods therefore do not propagate a ``ParameterNotFoundException`` when the parameter sought is not found but propagate a ``ParameterFormatException`` if the parameter is found but cannot be converted to the desired type.

* It is also possible to target a *test parameter* or a *global parameter* with specific methods. As with the previous methods, they are available in versions with and without default parameters. Here are a few examples:

``` java
ParameterService.INSTANCE.getTestString("paramName");
ParameterService.INSTANCE.getGlobalInt("paramName");
ParameterService.INSTANCE.getTestFloat("paramName", defaultValue);
ParameterService.INSTANCE.getGlobalBoolean("paramName", defaultValue);
```
### Simulate locally the parameter transmission by *OpenTestFactory* during the test design phase

During the design phase of your tests, you may want to run them locally before integrating them into an *OpenTestFactory* workflow. 
To do this, you will need to locally reproduce the processing performed by the orchestrator to make your parameters available in your test Java project.
Two actions are necessary:

* First, you need to drop an .ini file containing your test parameters on your filesystem at the same level as your test Java project. 
  This file will contain a section for global parameters and a section for test parameters. 
  Each parameter is described in the form of key (name of the variable) = value (value of the parameter)
  Below is an example of an .ini file:
  
``` ini
[global]
DS_GLOB_INT=987654321
DS_GLOB_BOOLEAN=false
DS_GLOB_FLOAT=45.12
DS_GLOB_DOUBLE=12223374512203.4658
DS_GLOB_LABEL=laptop
TC_COLOR=blue
[test]
DS_INT= 123456789
DS_FLOAT=12.42
DS_BOOLEAN=true
DS_DOUBLE=622337203685477580.4658
DS_LABEL=label
TC_COLOR=red
```

* Second, you need to create an environment variable on your operating system that allows the *Java Parameter Library* to find your parameters .ini file.
  This variable must be named "_SQUASH_TF_TESTCASE_PARAM_FILES" and must have as value the name of your .ini file.
  Here are two examples below, respectively for windows and linux:
  
``` cmd
setx _SQUASH_TF_TESTCASE_PARAM_FILES "myParameterFile.ini"
```
``` bash
export _SQUASH_TF_TESTCASE_PARAM_FILES=myParameterFile.ini
```

## Use without *OpenTestFactory*

The library can also be used in a test java project without *OpenTestFactory*. 
* In this case, the parameters must be entered in an .ini file accessible by the java project. Below is an example of an .ini file including test parameters and global parameters:

``` ini
[global]
DS_GLOB_INT=987654321
DS_GLOB_BOOLEAN=false
DS_GLOB_FLOAT=45.12
DS_GLOB_DOUBLE=12223374512203.4658
DS_GLOB_LABEL=laptop
TC_COLOR=blue
[test]
DS_INT= 123456789
DS_FLOAT=12.42
DS_BOOLEAN=true
DS_DOUBLE=622337203685477580.4658
DS_LABEL=label
TC_COLOR=red
```

* To access your parameters in your test classes, instantiate a ParameterService by passing it the path of your .ini file as a constructor parameter:

``` java
ParameterService paramService = new ParameterServiceImpl("/foo/bar/myParameterFile.ini");
```

* From this instance of ParameterService, you now have access to the same methods as those described in the part about running from *OpenTestFactory*. Below are some examples:

``` java
paramService.getString("paramName");
paramService.getInt("paramName");
paramService.getDouble("paramName", defaultValue);
paramService.getBoolean("paramName", defaultValue);
paramService.getTestString("paramName");
paramService.getGlobalInt("paramName");
paramService.getTestFloat("paramName", defaultValue);
paramService.getGlobalBoolean("paramName", defaultValue);
```

## License

Copyright 2021 Henix, henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.